[unix_http_server]
file=./tmp/supervisor.sock   ; (the path to the socket file)

[inet_http_server]         ; inet (TCP) server disabled by default
port=127.0.0.1:9010        ; (ip_address:port specifier, *:port for all iface)


[supervisord]
logfile=./logs/supervisord.log ; (main log file;default $CWD/supervisord.log)
logfile_maxbytes=50MB       ; (max main logfile bytes b4 rotation;default 50MB)
logfile_backups=10          ; (num of main logfile rotation backups;default 10)
loglevel=debug               ; (log level;default info; others: debug,warn,trace)
pidfile=./tmp/pids/supervisord.pid ; (supervisord pidfile;default supervisord.pid)
nodaemon=false              ; (start in foreground if true;default false)
minfds=1024                 ; (min. avail startup file descriptors;default 1024)
minprocs=200                ; (min. avail process descriptors;default 200)

[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface

;[rpcinterface:twiddler]
;supervisor.rpcinterface_factory = supervisor_twiddler.rpcinterface:make_twiddler_rpcinterface

[supervisorctl]
serverurl=http://localhost:9010 ; use an http:// url to specify an inet socket

[program:ttc_multi_worker]
numprocs=1
numprocs_start=1
process_name=%(program_name)s_%(process_num)s
command=twistd -n
    --pidfile=./tmp/pids/%(program_name)s_%(process_num)s.pid
    start_worker
    --worker-class=vusion.VusionMultiWorker
    --vhost=/develop
    --config=./etc/ttc_multiworker.yaml
stdout_logfile=./logs/%(program_name)s_%(process_num)s.log
stdout_logfile_maxbytes=10MB
stdout_logfile_backups=10
stderr_logfile=./logs/%(program_name)s_%(process_num)s.err
stderr_logfile_maxbytes=10MB
stderr_logfile_backups=10
autorestart=true

[program:ttc_dispatcher]
numprocs=1
numprocs_start=1
process_name=%(program_name)s_%(process_num)s
command=twistd -n
    --pidfile=./tmp/pids/%(program_name)s_%(process_num)s.pid
    start_worker
    --worker-class=dispatchers.DynamicDispatchWorker
    --vhost=/develop
    --config=./etc/ttc_dispatcher.yaml
stdout_logfile=./logs/%(program_name)s_%(process_num)s.log
stdout_logfile_maxbytes=10MB
stdout_logfile_backups=10
stderr_logfile=./logs/%(program_name)s_%(process_num)s.err
stderr_logfile_maxbytes=10MB
stderr_logfile_backups=10
autorestart=true

[program:ttc_garbage]
numprocs=1
numprocs_start=1
process_name=%(program_name)s_%(process_num)s
command=twistd -n
    --pidfile=./tmp/pids/%(program_name)s_%(process_num)s.pid
    start_worker
    --worker-class=vusion.GarbageWorker
    --vhost=/develop
    --config=./etc/ttc_garbage_worker.yaml
stdout_logfile=./logs/%(program_name)s_%(process_num)s.log
stdout_logfile_maxbytes=10MB
stdout_logfile_backups=10
stderr_logfile=./logs/%(program_name)s_%(process_num)s.err
stderr_logfile_maxbytes=10MB
stderr_logfile_backups=10
autorestart=true

[program:transport_8282]
numprocs=1
numprocs_start=1
process_name=%(program_name)s_%(process_num)s
command=twistd -n
    --pidfile=./tmp/pids/%(program_name)s_%(process_num)s.pid
    start_worker
    --worker-class=transports.YoUgHttpTransport
    --vhost=/develop
    --config=./etc/config/ttc_yo_transport_8282.yaml
stdout_logfile=./logs/%(program_name)s_%(process_num)s.log
stdout_logfile_maxbytes=10MB
stdout_logfile_backups=10
stderr_logfile=./logs/%(program_name)s_%(process_num)s.err
stderr_logfile_maxbytes=10MB
stderr_logfile_backups=10
autorestart=true

[program:transport_8181]
numprocs=1
numprocs_start=1
process_name=%(program_name)s_%(process_num)s
command=twistd -n
    --pidfile=./tmp/pids/%(program_name)s_%(process_num)s.pid
    start_worker
    --worker-class=transports.YoUgHttpTransport
    --vhost=/develop
    --config=./etc/config/ttc_yo_transport_8181.yaml
stdout_logfile=./logs/%(program_name)s_%(process_num)s.log
stdout_logfile_maxbytes=10MB
stdout_logfile_backups=10
stderr_logfile=./logs/%(program_name)s_%(process_num)s.err
stderr_logfile_maxbytes=10MB
stderr_logfile_backups=10
autorestart=true

[program:transport_3669]
numprocs=1
numprocs_start=1
process_name=%(program_name)s_%(process_num)s
command=twistd -n
    --pidfile=./tmp/pids/%(program_name)s_%(process_num)s.pid
    start_worker
    --worker-class=transports.CmTransport
    --vhost=/develop
    --config=./etc/config/ttc_cm_transport_3669.yaml
stdout_logfile=./logs/%(program_name)s_%(process_num)s.log
stdout_logfile_maxbytes=10MB
stdout_logfile_backups=10
stderr_logfile=./logs/%(program_name)s_%(process_num)s.err
stderr_logfile_maxbytes=10MB
stderr_logfile_backups=10
autorestart=true

[program:transport_0021]
numprocs=1
numprocs_start=1
process_name=%(program_name)s_%(process_num)s
command=twistd -n
    --pidfile=./tmp/pids/%(program_name)s_%(process_num)s.pid
    start_worker
    --worker-class=transports.CmYoTransport
    --vhost=/develop
    --config=./etc/config/ttc_cm_yo_transport_0031642500021.yaml
stdout_logfile=./logs/%(program_name)s_%(process_num)s.log
stdout_logfile_maxbytes=10MB
stdout_logfile_backups=10
stderr_logfile=./logs/%(program_name)s_%(process_num)s.err
stderr_logfile_maxbytes=10MB
stderr_logfile_backups=10
autorestart=true

;[program:api_transport]
;numprocs=1
;numprocs_start=1
;process_name=%(program_name)s_%(process_num)s
;command=twistd -n
;    --pidfile=./tmp/pids/%(program_name)s_%(process_num)s.pid
;    start_worker
;    --worker-class=vumi.transports.api.HttpApiTransport
;    --vhost=/develop
;    --set-option=web_port:9040
;    --set-option=web_path:sms
;stdout_logfile=./logs/%(program_name)s_%(process_num)s.log
;stdout_logfile_maxbytes=10MB
;stdout_logfile_backups=10
;stderr_logfile=./logs/%(program_name)s_%(process_num)s.err
;stderr_logfile_maxbytes=10MB
;stderr_logfile_backups=10
;autorestart=true
